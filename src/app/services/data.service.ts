import {Injectable} from '@angular/core';
import {HttpClient, HttpParams} from '@angular/common/http';
import {Customer, ICustomer} from '../models/customer';
import {Product} from '../models/product';
import {catchError, retry, tap} from 'rxjs/operators';
import {Observable} from 'rxjs';


import * as faker from 'faker';
import {Order} from '../models/order';

@Injectable({
  providedIn: 'root'
})
export class DataService {
  // private REST_API_SERVER = 'http://localhost:5000';
  private REST_API_SERVER = 'http://64.225.102.15:8080';
  // private CUSTOMER_REST_API_SERVER = 'http://localhost:5000';
  // private REST_API_SERVER = 'http://142.93.175.214:8080';
  // private CUSTOMER_REST_API_SERVER = 'http://142.93.175.214:2020';
  // private CRM_REST_API_SERVER = 'http://142.93.175.214:4040';

  constructor(private httpClient: HttpClient) {
  }

  public getCustomers(errorHandlerCallback): Observable<Customer[]> {
    return this.httpClient.get<ICustomer[]>(this.REST_API_SERVER + '/customer')
      // return this.httpClient.get<Customer[]>(this.REST_API_SERVER + '/customer');
      .pipe(
        catchError(errorHandlerCallback('searchCustomerPhoneNumber', 'aaa'))
      );
  }

  public searchCustomerByMSISDN(msisdn: string, errorHandlerCallback): Observable<Customer[]> {
    return this.httpClient.get<ICustomer[]>(this.REST_API_SERVER + '/customer/search' + '?msisdn=' + msisdn)
      // return this.httpClient.get<Customer[]>(this.REST_API_SERVER + '/customer');
      .pipe(
        catchError(errorHandlerCallback)
      );
  }

  public getCustomer(id: string, errorHandlerCallback): Observable<Customer> {
    return this.httpClient.get<ICustomer>(this.REST_API_SERVER + '/customer/' + id)
      // return this.httpClient.get<Customer[]>(this.REST_API_SERVER + '/customer');
      .pipe(
        catchError(errorHandlerCallback)
      );
  }

  public getPlanList(errorHandlerCallback): Observable<Product[]> {
    return this.httpClient.get<Product[]>(this.REST_API_SERVER + '/product')
      // return this.httpClient.get<Customer[]>(this.REST_API_SERVER + '/customer');
      .pipe(
        catchError(errorHandlerCallback)
      );
  }

  public getProduct(id: string, errorHandlerCallback): Observable<Product> {
    return this.httpClient.get<ICustomer>(this.REST_API_SERVER + '/product/' + id)
      // return this.httpClient.get<Customer[]>(this.REST_API_SERVER + '/customer');
      .pipe(
        catchError(errorHandlerCallback)
      );
  }

  public getPlan(id: string, errorHandlerCallback): Observable<Product> {
    return this.httpClient.get<Product>(this.REST_API_SERVER + '/product/' + id)
      // return this.httpClient.get<Customer[]>(this.REST_API_SERVER + '/customer');
      .pipe(
        catchError(errorHandlerCallback)
      );
  }

  public createOrder(customerId: string, errorHandlerCallback): Observable<Order> {
    return this.httpClient.post<Order>(this.REST_API_SERVER + '/order/' + customerId, customerId)
      .pipe(
        catchError(errorHandlerCallback)
      );
  }

  public cancelOrder(customerId: string, errorHandlerCallback): Observable<Order> {
    return this.httpClient.delete<Order>(this.REST_API_SERVER + '/order/cancel/' + customerId)
      .pipe(
        catchError(errorHandlerCallback)
      );
  }

  public addToOrder(customerId: string, productId: string, errorHandlerCallback): Observable<Order> {
    // /order/product/{customerId}/{productId}
    return this.httpClient.put<Order>(this.REST_API_SERVER + '/order/product/' + customerId + '/' + productId, customerId)
      .pipe(
        catchError(errorHandlerCallback)
      );
  }

  // /order/product/{customerId}/{productId}

  public removeFromOrder(customerId: string, productId: string, errorHandlerCallback): Observable<Order> {
    // /order/product/{customerId}/{productId}
    return this.httpClient.delete<Order>(this.REST_API_SERVER + '/order/product/' + customerId + '/' + productId)
      .pipe(
        catchError(errorHandlerCallback)
      );
  }


  public postOrderData(order: Order, errorHandlerCallback): Observable<Order> {
    return this.httpClient.post<Order>(this.REST_API_SERVER + '/order', order)
      .pipe(
        catchError(errorHandlerCallback)
      );
  }

  public getOrder(customerId: string, errorHandlerCallback): Observable<Order> {
    return this.httpClient.get<Order>(this.REST_API_SERVER + '/order/' + customerId)
      // return this.httpClient.get<Customer[]>(this.REST_API_SERVER + '/customer');
      .pipe(
        catchError(errorHandlerCallback)
      );
  }

}

