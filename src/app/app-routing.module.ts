import {NgModule} from '@angular/core';
import {Routes, RouterModule} from '@angular/router';
import {CustomerSelectionComponent, DialogOrderCompleteDialog} from './screens/customer-selection/customer-selection.component';
import {OrderDetailsComponent} from './screens/order-details/order-details.component';


const routes: Routes = [
  {path: '', redirectTo: 'customer-selection', pathMatch: 'full'},
  {path: 'customer-selection', component: CustomerSelectionComponent},
  {path: 'order-details', component: OrderDetailsComponent},
  {path: 'order-details/:id', component: OrderDetailsComponent},
  {path: '', component: DialogOrderCompleteDialog},
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule {
}
