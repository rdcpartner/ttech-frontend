import {Component, Inject, OnInit, ViewChild} from '@angular/core';
import {FormBuilder, FormControl, FormGroup, Validators} from '@angular/forms';
import {DataService} from '../../services/data.service';
import {ActivatedRoute, Router} from '@angular/router';
import {HttpErrorResponse} from '@angular/common/http';
import {throwError} from 'rxjs';
import {Customer} from '../../models/customer';
import {MAT_DIALOG_DATA, MatDialog, MatDialogRef, MatStepper, MatTableDataSource} from '@angular/material';
import {DatePipe} from '@angular/common';
import {Product} from '../../models/product';
import {Order} from '../../models/order';

@Component({
  selector: 'app-customer-selection',
  templateUrl: './customer-selection.component.html',
  styleUrls: ['./customer-selection.component.scss']
})
export class CustomerSelectionComponent implements OnInit {
  private isLinear = true;
  private selectCustomerFormGroup: FormGroup;
  private selectProductFormGroup: FormGroup;
  private makeOrderFormGroup: FormGroup;
  private finaliseOrderFormGroup: FormGroup;
  private customerMSISDNQueryValue: string;
  dataSource: MatTableDataSource<DataTableItem>;
  displayedColumns: string[] = ['position', 'msisdn', 'name', 'surname'];
  columnHeaders: string[] = ['', 'MSISDN', 'Ad', 'Soyad'];

  private ELEMENT_DATA: DataTableItem[] = [];
  selectedElement: DataTableItem | null;
  private products: Array<Product> = [];

  // For selecting on table. Selection is finalised after tapping next
  private tmpSelectedCustomer: Customer;
  private selectedCustomer: Customer;
  private selectedPlan: Product;
  private cart: Array<Product> = [];
  private order: Order;

  @ViewChild('stepper', {static: false}) stepper: MatStepper;

  constructor(public dialog: MatDialog, private route: ActivatedRoute, private router: Router,
              private dataService: DataService, private formBuilder: FormBuilder) {
  }

  ngOnInit() {
    this.selectCustomerFormGroup = this.formBuilder.group({
        customerMSISDNQueryValueFormCtrl: [''],
        customerMSISDNFormCtrl: [''],
      }
    );
    this.selectCustomerFormGroup.get('customerMSISDNQueryValueFormCtrl').valueChanges.subscribe(value => {
      // Already on ngmodel
      this.customerMSISDNQueryValue = value;
    });
    this.selectProductFormGroup = this.formBuilder.group({
        productFormCtrl: ['']
      }
    );
    this.makeOrderFormGroup = this.formBuilder.group({
      // thirdCtrl: ['', Validators.required]
    });

    this.finaliseOrderFormGroup = this.formBuilder.group({
        fourthCtrl: ['']
      }
    );


  }

  onClearData() {
    this.emptyCart();
    this.cart = [];
    this.selectedPlan = null;
    this.selectedCustomer = null;
    this.tmpSelectedCustomer = null;
    this.products = [];
    this.ELEMENT_DATA = [];
    this.dataSource = new MatTableDataSource<DataTableItem>(this.ELEMENT_DATA);
    this.selectedElement = null;
    this.stepper.reset();

  }

  onSearchCustomer() {
    console.log('Test');
    console.log(this.customerMSISDNQueryValue);
    this.searchCustomer(this.customerMSISDNQueryValue);
  }

  searchCustomer(msisdn: string) {
    this.dataService.searchCustomerByMSISDN(msisdn, this.handleAPIHTTPErrors).subscribe((data: Array<Customer>) => {
        // console.log(data);
        // console.log(data.toString());
        if (data.length > 0) {
          console.log(data);
          this.convertToTableData(data);
        } else {
          console.log('Cannot find the customer by phone');
          this.ELEMENT_DATA = [];
          this.dataSource = new MatTableDataSource<DataTableItem>(this.ELEMENT_DATA);
        }
      }
    );
  }

  customerSelected(customer: Customer) {
    console.log('Customer Selected');
    console.log(customer.msisdn);
    console.log(customer);
    this.tmpSelectedCustomer = customer;
    // this.router.navigate(['/product-selection']);
  }

  customerSelectedNext() {
    // this.firstFormGroup.controls['firstCtrl'].setErrors(null)
    this.selectedCustomer = this.tmpSelectedCustomer;
    this.dataService.getOrder(this.selectedCustomer.id, (error: HttpErrorResponse) => {
        console.log(error);
        console.log(error.status);
        if (error.status === 404) {
          this.stepper.next();
        }
      }
    ).subscribe((data: Order) => {
        console.log(data);
        console.log(data.toString());
        this.order = data;
        if (this.order !== undefined) {
          for (const product of this.order.products) {
            console.log(product);
            // let tmpOrder = this.order[i] as Order;
            // console.log(tmpOrder);
            // // Check if customer is the selected customer
            this.dataService.getProduct(product.toString(), this.handleAPIHTTPErrors).subscribe((dataProduct: Product) => {
              console.log(dataProduct);
              // let orderData: OrderData = {
              //   active: order.active,
              //   id: order.id,
              //   productName: data.name,
              //   staticIpAddress: order.staticIpAddress
              // };
              // console.log(orderData);
              this.order.products.push(dataProduct);
              this.cart.push(dataProduct);
            });
            console.log(this.order);
            // this.stepper.next();
          }
          this.stepper.selectedIndex = 2;
        } else {
          this.stepper.next();
        }
      }
    );


  }


  getPlanList() {
    this.dataService.getPlanList(this.handleAPIHTTPErrors).subscribe((data: Array<Product>) => {
      // console.log(data);
      this.products = data;
    });
  }

  selectPlan(product: Product) {
    console.log('Plan Selected');
    console.log(product);
    this.selectedPlan = product;
    // this.router.navigate(['/product-details/' + product.id]);
    // this.emptyCart();
    // this.addToCart(product);
    this.stepper.next();
  }


  async addToCart(product: Product) {
    //
    // this.emptyCart();
    if (this.cart.length > 0) {
      console.log('adding to cart');
      console.log(this.cart);
      this.dataService.addToOrder(this.selectedCustomer.id, this.selectedPlan.id, this.handleAPIHTTPErrors)
        .subscribe((data: Order) => {
          console.log(data);
          this.cart.push(product);
          // this.products = data;
        });


    } else {
      console.log('creating the order before adding to cart');
      console.log(this.cart);
      await this.dataService.createOrder(this.selectedCustomer.id, (error: HttpErrorResponse) => {
        console.log('Error on createOrder');
        console.log(error);
        // alert(error.statusText);
        if (error.status === 405) {
          this.emptyCart();
        }

      })
        .subscribe((data: Order) => {
          console.log(data);

          // this.products = data;
        });
      this.dataService.addToOrder(this.selectedCustomer.id, this.selectedPlan.id, this.handleAPIHTTPErrors)
        .subscribe((data: Order) => {
          console.log(data);
          this.cart.push(product);
          // this.products = data;
        });
    }

  }

  emptyCart() {
    console.log('Cancel Order');
    this.dataService.cancelOrder(this.selectedCustomer.id, this.handleAPIHTTPErrors)
      .subscribe((data: Order) => {
        console.log(data);
        this.cart = [];
        // this.products = data;
      });

  }

  removeProduct(product: Product) {
    console.log('Cancel Order');
    this.dataService.removeFromOrder(this.selectedCustomer.id, product.id, this.handleAPIHTTPErrors)
      .subscribe((data: Order) => {
        console.log(data);
        const index = this.cart.indexOf(product, 0);
        if (index > -1) {
          this.cart.splice(index, 1);
        }
      });
  }

  finalizeOrder() {
    this.order = new Order();
    this.order.products = this.cart;
    this.order.customerId = this.selectedCustomer.id;
    console.log(this.order);
    this.dataService.postOrderData(this.order, this.handleAPIHTTPErrors).subscribe((data: Order) => {
      console.log(data);
      this.order = data;

      const dialogRef = this.dialog.open(DialogOrderCompleteDialog, {
        width: '250px',
        data: {titleText: 'Sipariş Tamamlandı'}
      });
      dialogRef.afterClosed().subscribe(result => {
        this.router.navigate(['/order-details/' + this.order.customerId]);
      });
    });

  }


  handleAPIHTTPErrors(error: HttpErrorResponse) {
    // TODO: Parse error message and Show alert to user
    // TODO: Make error handling app wide instead of component based
    console.log('Error fetching data');
    if (error.error instanceof ErrorEvent) {
      // A client-side or network error occurred. Handle it accordingly.
      console.error('An error occurred:', error.error.message);
    } else {
      // The backend returned an unsuccessful response code.
      // The response body may contain clues as to what went wrong,
      console.error(
        `Backend returned code ${error.status}, ` +
        `body was: ${error.error}`);
    }
    // return an observable with a user-facing error message
    return throwError(
      'Something bad happened; please try again later.');
  }


  convertToTableData(data: any) {
    this.ELEMENT_DATA = [];
    let index = 0;
    const datePipe = new DatePipe('tr-TR');
    for (const item of data) {

      index++;

      const dataTableItem: DataTableItem = {
        customer: item,
        position: index,
        name: item.name,
        surname: item.surname,
        msisdn: item.msisdn
      };

      this.ELEMENT_DATA.push(dataTableItem);
      this.dataSource = new MatTableDataSource<DataTableItem>(this.ELEMENT_DATA);
    }
    console.log(this.ELEMENT_DATA);
  }

  stepperChange(event: any) {
    switch (event.selectedIndex) {
      case 0:
        // Reload our customer data
        console.log('Customer Selection');
        // this.getCustomerList();
        break;
      case 1:
        console.log('Plan Selection');
        this.getPlanList();
        break;
      case 2:
        // console.log(this.selectedCustomer);
        // console.log(this.selectedProduct);
        break;
      case 3:
        // this.getStaticIPAddress();
        break;
      default:
        console.log('Error on stepper');
    }
  }

}


export interface DataTableItem {
  customer: Customer;
  position: number;
  name: string;
  surname: string;
  msisdn: string;
}

@Component({
  // tslint:disable-next-line:component-selector
  selector: 'dialog-order-complete',
  templateUrl: 'dialog-order-complete.html',
})
// tslint:disable-next-line:component-class-suffix
export class DialogOrderCompleteDialog {
  constructor(
    public dialogRef: MatDialogRef<DialogOrderCompleteDialog>,
    @Inject(MAT_DIALOG_DATA) public data: DialogTextData,
    // @Inject(MAT_DIALOG_DATA) public staticIpAddress: string,
    // @Inject(MAT_DIALOG_DATA) public titleText: string,
  ) {
  }

  onClick(): void {
    this.dialogRef.close();
  }

}

export interface DialogTextData {
  titleText: string;
  productData: string;
}
