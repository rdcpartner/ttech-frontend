import {Component, OnInit} from '@angular/core';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';
import {MatDialog} from '@angular/material/dialog';
import {ActivatedRoute, ParamMap, Router} from '@angular/router';
import {DataService} from '../../services/data.service';
import {Customer} from '../../models/customer';
import {HttpErrorResponse} from '@angular/common/http';
import {throwError} from 'rxjs';
import {switchMap} from 'rxjs/operators';
import {Order} from '../../models/order';
import {Product} from '../../models/product';


@Component({
  selector: 'app-order-details',
  templateUrl: './order-details.component.html',
  styleUrls: ['./order-details.component.scss']
})
export class OrderDetailsComponent implements OnInit {
  private customerId: string;
  private order: Order;
  private orderIdFormGroup: FormGroup;
  private ordersProductData: Array<Product> = [];

  constructor(private route: ActivatedRoute, public dialog: MatDialog,
              private router: Router, private dataService: DataService, private formBuilder: FormBuilder) {
  }

  ngOnInit() {
    this.customerId = this.route.snapshot.paramMap.get('id');
    console.log('init');
    console.log(this.customerId);
    if (this.customerId) {
      this.getOrdersById(this.customerId);
    }
  }

  getOrdersById(id: string) {
    this.dataService.getOrder(id, this.handleAPIHTTPErrors).subscribe((data: Order) => {
        console.log(data);
        console.log(data.toString());
        this.order = data;
        this.ordersProductData = [];
        for (const product of this.order.products) {
          console.log(product);
          // let tmpOrder = this.order[i] as Order;
          // console.log(tmpOrder);
          // // Check if customer is the selected customer
          this.dataService.getProduct(product.toString(), this.handleAPIHTTPErrors).subscribe((dataProduct: Product) => {
            console.log(dataProduct);
            // let orderData: OrderData = {
            //   active: order.active,
            //   id: order.id,
            //   productName: data.name,
            //   staticIpAddress: order.staticIpAddress
            // };
            // console.log(orderData);
            this.ordersProductData.push(dataProduct);
          });

        }
      }
    );
  }

  onOrderDetails() {
    console.log(this.customerId);
    this.router.navigate(['/order-details/' + this.customerId]);
  }

  handleAPIHTTPErrors(error: HttpErrorResponse) {
    // TODO: Parse error message and Show alert to user
    // TODO: Make error handling app wide instead of component based
    console.log('Error fetching data');
    if (error.error instanceof ErrorEvent) {
      // A client-side or network error occurred. Handle it accordingly.
      console.error('An error occurred:', error.error.message);
    } else {
      // The backend returned an unsuccessful response code.
      // The response body may contain clues as to what went wrong,
      console.error(
        `Backend returned code ${error.status}, ` +
        `body was: ${error.error}`);
    }
    // return an observable with a user-facing error message
    return throwError(
      'Something bad happened; please try again later.');
  }
}

