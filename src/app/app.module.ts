import {BrowserModule} from '@angular/platform-browser';
import {NgModule, LOCALE_ID} from '@angular/core';

import {registerLocaleData} from '@angular/common';
import localeTr from '@angular/common/locales/tr';

registerLocaleData(localeTr);


import {AppRoutingModule} from './app-routing.module';
import {AppComponent} from './app.component';
import {HttpClientModule} from '@angular/common/http';
import {DialogOrderCompleteDialog, CustomerSelectionComponent} from './screens/customer-selection/customer-selection.component';
import {
  MatButtonModule, MatCardModule, MatCheckboxModule, MatDialogModule, MatDividerModule,
  MatFormFieldModule, MatIconModule,
  MatInputModule,
  MatProgressSpinnerModule, MatSelectModule,
  MatStepperModule, MatTableModule,
  MatToolbarModule
} from '@angular/material';
import {BrowserAnimationsModule} from '@angular/platform-browser/animations';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';

import {OrderDetailsComponent} from './screens/order-details/order-details.component';


@NgModule({
  declarations: [
    AppComponent,
    CustomerSelectionComponent,
    OrderDetailsComponent,
    DialogOrderCompleteDialog,
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    BrowserAnimationsModule,
    HttpClientModule,
    MatToolbarModule,
    MatIconModule,
    MatCardModule,
    MatButtonModule,
    MatProgressSpinnerModule,
    MatStepperModule,
    FormsModule,
    ReactiveFormsModule,
    MatFormFieldModule,
    MatInputModule,
    MatSelectModule,
    MatCheckboxModule,
    MatDialogModule,
    MatDividerModule,
    MatTableModule
  ],
  providers: [
    {provide: LOCALE_ID, useValue: 'tr-TR'},
  ],
  bootstrap: [AppComponent]
})
export class AppModule {
}
