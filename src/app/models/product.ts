export interface IProduct {
  id?: string;
  name: string;
  description?: string;
  price: number;
  pricePeriod: string;
  active: boolean;
}

export class Product implements IProduct {
  id?: string;
  description?: string;
  name: string;
  price: number;
  pricePeriod: string;
  active: boolean;
}


// [{"id":"900f93d9-c9da-4bca-92a5-9e424729b1e2",
// "name":"Fizy",
// "price":19.99,"pricePeriod":"Monthly","active":true},
// {"id":"aa0f8c6f-fa28-417a-a6af-5a5884df005e",
// "name":"TV+","price":9.99,"pricePeriod":"Monthly","active":true},
// {"id":"f4b0bf86-fa0a-4bbc-85ce-7857aee761ae","name":"Dergilik","price":29.99,"pricePeriod":"Monthly","active":true}]
