import {Customer} from './customer';
import {Product} from './product';

export interface IOrder {
  customerId: string;
  // customer: Customer;
  products?: Array<Product>;
}

export class Order implements IOrder {
  // customer: Customer;
  customerId: string;
  products?: Array<Product>;
}
