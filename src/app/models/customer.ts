export interface ICustomer {
  id: string;
  name: string;
  surname: string;
  msisdn: string;
  active: boolean;
}

export class Customer implements ICustomer {
  id: string;
  name: string;
  surname: string;
  msisdn: string;
  active: boolean;
}


// [{"id":"919b21c4-ef2b-4d8b-929a-ec23730918b8","name":"Bruce","surname":"Lee",
// "msisdn":"5321111111","createDate":[2020,2,17,9,31,50,156000000],
// "updateDate":[2020,2,17,9,31,50,156000000],"active":true}]

