let faker=require('faker/locale/tr');


let database = {  customers:[], plans: [], orders:[]};

for (let i=0;i<9;i++){
  database.plans.push({
    id: faker.random.uuid().replace(/-/g,10).substr(0,32),
    name: faker.commerce.productName(),
    description: faker.lorem.sentences(),
    price: faker.commerce.price()
  })
}


for (let i=0; i<20;i++){
  database.customers.push({
    id: faker.random.uuid().replace(/-/g,10).substr(0,32),
    firstName: faker.name.firstName(),
    lastName: faker.name.lastName(),
    email: faker.internet.email(),
    phoneNumber: faker.phone.phoneNumber("53########")
  })
}

// for (let i=0; i<20;i++){
//   database.logs.push({
//     id: faker.random.uuid().replace(/-/g,10).substr(0,32),
//     firstNameNew: faker.name.firstName(),
//     firstNameOld: faker.name.firstName(),
//     lastNameNew: faker.name.lastName(),
//     lastNameOld: faker.name.lastName(),
//     staticIpAddressOld: faker.internet.ip(),
//     staticIpAddressNew: faker.internet.ip()
//   })
// }


console.log(JSON.stringify(database, null, 4));
